function getInput()
{
    var x = document.getElementById("demo2").value
    console.log(x)
}
function isNum()
{
    var x = document.getElementById("demo2").value
    if(isNaN(x)||x.replace(/(^\s*)|(\s*$)/g,"") == "")
    {
        alert("not number")
    }
}
function findString()
{
    var x = document.getElementById("demo3").value
    var patt = /[\w]+/g
    var str = x.replace(patt, "xxx")
    console.log(str)
}
function try_catch()
{
    try {
        var x = document.getElementById().value
    } catch (error) {
        alert(error)
        console.log(error)
    }
}

function hex()
{
    var hex = document.getElementById("hex").value;
    var dec = parseInt(hex, 16);
    var bin = dec.toString(2);
    document.getElementById("dec").value = dec;
    document.getElementById("bin").value = bin;
}

function dec()
{
    var dec = document.getElementById("dec").value;
    var dec = parseInt(dec);
    var bin = dec.toString(2);
    var hex = dec.toString(16);
    document.getElementById("hex").value = hex;
    document.getElementById("bin").value = bin;
}

function bin()
{
    var bin = document.getElementById("bin").value;
    var dec = parseInt(bin, 2);
    var hex = dec.toString(16);
    document.getElementById("dec").value = dec;
    document.getElementById("hex").value = hex;
}

function ascii()
{
    var ascii = document.getElementById("ascii").value;
    console.log(ascii, typeof ascii);
    var str = "";
    if(ascii.length % 2)
    {
        return;
    }
    for(var i=0;i<ascii.length; i=i+2)
    {
        var hex = ascii[i]+ascii[i+1];
        var dec = parseInt(hex, 16);
        str += String.fromCharCode(dec);
    }
    document.getElementById("words").value = str;
}

function words()
{
    var words = document.getElementById("words").value;
    var str = "";
    for(var i=0; i< words.length; i++)
    {
        var dec = words.charCodeAt(i);
        str += dec.toString(16);
    }
    document.getElementById("ascii").value = str;
}