function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex_light = 1;
showSlides_light(slideIndex_light);

function plusSlides_light(n) {
  showSlides_light(slideIndex_light += n);
}

function currentSlide_light(n) {
  showSlides_light(slideIndex_light = n);
}

function showSlides_light(n) {
  let i;
  let slides = document.getElementsByClassName("lightmySlides");
  let dots = document.getElementsByClassName("demo");
  let captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex_light = 1}
  if (n < 1) {slideIndex_light = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex_light-1].style.display = "block";
  dots[slideIndex_light-1].className += " active";
  captionText.innerHTML = dots[slideIndex_light-1].alt;
}